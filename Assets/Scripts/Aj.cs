﻿using UnityEngine;
using System.Collections;

public class Aj: MonoBehaviour
{

	public Animator anim;
	private float speedF;
	private float turnR;
	private float turnL;
	private float back;
	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.W)) {
			speedF += 1.0f;
		} else if(Input.GetKey(KeyCode.D)){
			turnR += 1.0f;
		}
		else if(Input.GetKey(KeyCode.A)){
			turnL += 1.0f;
		}
		else if(Input.GetKey(KeyCode.S)){
			back += 1.0f;
		}
		else{
			if (speedF > 0) {
				speedF = 0.0f;
			}
			if(turnR >0)
				turnR = 0.0f;
			if(turnL >0)
				turnL = 0.0f;
			if(back >0)
				back = 0.0f;
		}
		anim.SetFloat ("speed", speedF);
		anim.SetFloat ("turnR", turnR);
		anim.SetFloat ("turnL", turnL);
		anim.SetFloat ("back", back);
	}
}