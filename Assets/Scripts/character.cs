﻿using UnityEngine;
using System.Collections;

public class character : MonoBehaviour
{

	public Animator anim;
	private float speedF;
	private float turnR;
	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.W)) {
			speedF += 0.2f;
		} else if(Input.GetKey(KeyCode.D)){
			turnR += 0.2f;
		}
		else{
			if (speedF > 0) {
				speedF -= 0.8f;
			}
			if(turnR >0)
				turnR -= 0.8f;
			}
		anim.SetFloat ("speed", speedF);
		anim.SetFloat ("turnR", turnR);
	}
}